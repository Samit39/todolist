<?php

use MatthiasMullie\Minify;
/** 
 * Helper functions to minify css and js and save to the existing directory
 * 
**/

function minifyCss($cssFilePathArray)
{
	$minifiedOutputCssPath = 'assets/css/minified/';
	$toReturnArray = array();
	if (!empty($cssFilePathArray)) {
		foreach ($cssFilePathArray as $value) {
			$filename = getFileNameFromPath($value, ".css");
			$minifier = new Minify\CSS($value);
			// save minified file to disk
			$minifiedPath = $minifiedOutputCssPath . $filename;
			$minifier->minify($minifiedPath);
			//adding minified path to return arrrays to
			array_push($toReturnArray, $minifiedPath);
		}
	}
	return $toReturnArray;
}

function minifyJs($jsFilePathArray)
{
	$minifiedOutputJsPath = 'assets/js/minified/';
	$toReturnArray = array();
	if (!empty($jsFilePathArray)) {
		foreach ($jsFilePathArray as $value) {
			$filename = getFileNameFromPath($value, ".js");
			$minifier = new Minify\JS($value);
			// save minified file to disk
			$minifiedPath = $minifiedOutputJsPath . $filename;
			$minifier->minify($minifiedPath);
			//adding minified path to return arrrays to
			array_push($toReturnArray, $minifiedPath);
		}
	}
	return $toReturnArray;
}

function getFileNameFromPath($filePath, $extension)
{
	$filePathArray = explode('/', $filePath);
	foreach ($filePathArray as $value) {
		if (strpos($value, $extension)) {
			return $value;
		}
	}
}
