<?php

function pr($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function segment($segment)
{
    $ci = &get_instance();
    $value = $ci->uri->segment($segment);
    return $value;
}
