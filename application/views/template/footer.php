</body>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/landing.js"></script> -->
<!-- Adding Custom JS -->
<?php if (isset($addJS) && !empty($addJS)) {
		foreach ($addJS as $js) { ?>
			<script src="<?php echo base_url().$js; ?>"></script>
	<?php }
	} ?>
	<!-- Adding Custom JS ends-->
</html>
