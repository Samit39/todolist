<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE HTML>
<html>

<head>
	<title>Todo List - Practical Task Demonstration</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<!-- Adding Custom Css -->
	<?php if (isset($addCss) && !empty($addCss)) {
		foreach ($addCss as $css) { ?>
			<link href="<?php echo base_url().$css; ?>" rel="stylesheet" type="text/css" />
	<?php }
	} ?>
	<!-- Adding Custom Css ends-->
</head>

<body>
