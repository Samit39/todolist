<div class="container">
	<div class="row">
		<?php if (isset($success) && $success != '') { ?>
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>Success!</strong> <?php echo $success; ?>
			</div>
		<?php } ?>
		<?php if (isset($error) && $error != '') { ?>
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>Error!</strong> <?php echo $error; ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-md-10">
			<h1>To Do List <?php echo (isset($header_title) && $header_title != '') ? " - " . $header_title : ""; ?></h1>
		</div>
		<div class="col-md-2 view-all-div">
			<a href="<?php echo base_url();  ?>" type="button" class="btn btn-info pull-right"><i class="fa fa-eye"></i> View All Items</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-aqua">
				<div class="box-header">

				</div>

				<div class="box-body">
					<?php
					$list_id = segment(3);
					if ($list_id != '') {
						$form_submit_url = base_url() . 'todo/create/' . $list_id;
					} else {
						$form_submit_url = base_url() . 'todo/create';
					}
					$attributes = array(
						'id' => "todo_form",
						'class' => "form-horizontal",
						'name' => "todo_form",
						'autocomplete' => "off"
					);

					echo form_open($form_submit_url, $attributes);

					if (validation_errors() != '') {
						$title = set_value('title');
						$description = set_value('description');
						$completed = set_value('completed');
						$date = set_value('date');
					} else if (isset($item_data) && !empty($item_data)) {
						$title = $item_data->title;
						$description = $item_data->description;
						$completed = $item_data->completed;
						$date = $item_data->date;
					} else {
						$title = "";
						$description = "";
						$completed = "";
						$date = "";
					}
					?>
					<div class="form-group form-group-md">
						<label for="title" class="col-xs-3 control-label">Title*</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="title" name="title" value="<?php echo $title; ?>" placeholder="Please enter title of task here...">
							<span class="form-error"><?php echo form_error('title'); ?></span>
						</div>
					</div>
					<div class="form-group form-group-md">
						<label for="description" class="col-xs-3 control-label">Description*</label>
						<div class="col-xs-9">
							<textarea class="form-control" rows="5" id="description" name="description" placeholder="Please enter description of task here..."><?php echo $description; ?></textarea>
							<span class="form-error"><?php echo form_error('description'); ?></span>
						</div>
					</div>
					<div class="form-group form-group-md">
						<label for="completed" class="col-xs-3 control-label">Completed</label>
						<div class="col-xs-9">
							<input class="" id="completed" type="checkbox" name="completed" <?php echo ($completed == 1) ? "checked" : "" ?>>
						</div>
					</div>
					<div class="form-group form-group-md">
						<label for="date" class="col-xs-3 control-label">Date*</label>
						<div class="col-xs-9">
							<div class="form-group form-group-md">
								<div class='input-group date' id='datetimepicker1'>
									<input type='text' class="form-control" id="date" name="date" value="<?php echo $date; ?>" readonly placeholder="Please select date here..." />
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							<span class="form-error"><?php echo form_error('date'); ?></span>
						</div>
					</div>
					<div class="form-group form-group-md">
						<button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-plus"></span> Submit</button>
					</div>
					</form>
				</div>
				<div class="box-footer clearfix no-border">
				</div>
			</div>
		</div>
	</div>
</div>
