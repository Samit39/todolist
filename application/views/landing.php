<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>To Do List <?php echo (isset($header_title) && $header_title != '') ? " - " . $header_title : ""; ?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-aqua">
				<div class="box-header">
				</div>

				<div class="box-body">
					<ul class="todo-list ui-sortable">
						<?php
						if (isset($all_data) && !empty($all_data)) {
							foreach ($all_data as $row) {
						?>
								<li>
									<div class="pull-right">
										Status
										<input type="checkbox" id="<?php echo $row->id; ?>" <?php echo ($row->completed == 1) ? "checked" : ""; ?>>
									</div>

									<Strong><span class="text"><?php echo $row->title; ?></span></Strong>
									<p><?php echo $row->description; ?></p>

									<small class="label label-primary"><?php echo $row->date; ?></small>

									<div class="tools">
										<a href="<?php echo base_url('todo/create/' . $row->id);  ?>" class="fa fa-edit" title="Edit"></a>
										<a href="javascript:void(0)" class="deleteClass fa fa-trash-o" title="Delete" id="<?php echo $row->id; ?>"></a>
									</div>
								</li>
							<?php
							}
						} else {
							?>
							<li>
								No items found, Please add item to display in the list.
							</li>
						<?php
						}
						?>
					</ul>
				</div>
				<div class="box-footer clearfix no-border">
					<a href="<?php echo base_url('todo/create/');  ?>" type="button" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add item</a>
				</div>
			</div>
		</div>
	</div>
</div>
