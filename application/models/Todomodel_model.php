<?php

class Todomodel_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

	//Validation rules for todolist form on create() function in todocontroller
    public function rules() {
        $array = array(
            array(
                'field' => 'title',
                'label' => 'title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'completed',
                'label' => 'completed',
                'rules' => '',
            ), 
            array(
                'field' => 'date',
                'label' => 'date',
                'rules' => 'trim|required',
            ),  
        );
        return $array;
    }

}
