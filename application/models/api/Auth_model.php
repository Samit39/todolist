<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	protected $tokensTable = 'tokens';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//Add new data to database
	public function postTokens($data)
	{
		$this->db->insert($this->tokensTable, $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function checkIfRefreshTokenExists($refreshToken)
	{
		$this->db->where('refresh_tokens', $refreshToken);
		// $this->db->from($ttokensTable);
		$numOfRows = $this->db->get($this->tokensTable)->num_rows();
		if ($numOfRows >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public function removeRefreshToken($refreshToken)
	{
		$this->db->delete($this->tokensTable,array('refresh_tokens'=>$refreshToken));
		return $this->db->affected_rows();
	}
}
