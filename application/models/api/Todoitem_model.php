<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todoitem_model extends CI_Model {
	protected $todoItemsTable = 'todoitems';

	public function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	//Fetch data from database according to optional parameter id
	public function getTodoList($id) {
		if(!empty($id)){
            $data = $this->db->get_where($this->todoItemsTable, ['id' => $id])->row_array();
        }else{	
            $data = $this->db->get($this->todoItemsTable)->result();
        }
		return $data;
	}

	//Add new data to database
	public function postTodoList($data){
		$this->db->insert($this->todoItemsTable, $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	//Update data to database according to supplied id
	public function updateTodoList($data,$id){
		$this->db->where('id', $id);
		$this->db->update($this->todoItemsTable, $data);
		return $this->db->affected_rows();
	}

	//Delete data from database according to supplied id
	public function deleteTodoList($id){
		$this->db->delete($this->todoItemsTable,array('id'=>$id));
		return $this->db->affected_rows();
	}
}
