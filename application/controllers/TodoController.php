<?php
defined('BASEPATH') or exit('No direct script access allowed');


class TodoController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('minify');
		$this->load->model('Todomodel_model', 'todo');
	}

	//Index function to display all datas by fetching from API
	public function index()
	{
		$this->data['header_title'] = "All Items";
		$this->headerData['addCss'] = minifyCss(
			array(
				'assets/css/style.css'
			)
		);
		$this->footerData['addJs'] = minifyJs(
			array(
				'assets/js/landing.js'
			)
		);
		//
		// Get cURL resource
		$curl = curl_init();
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/practicaltaskci/rest-api/todoitem';
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		if (curl_error($curl)) {
			$returndata = curl_error($curl);
			$this->data['error'] = 'Unable to process your request at this time. Please try again later or contact system administrator for further assistance.';
		} else {
			$returndata = json_decode($resp);
		}
		$this->data['all_data'] = $returndata;
		// Close request to clear up some resources
		curl_close($curl);
		//
		$this->load->view('template/header', $this->headerData);
		$this->load->view('landing', $this->data);
		$this->load->view('template/footer', $this->footerData);
	}

	//adding or Updating any data is handled by create() function
	public function create()
	{
		$this->data = array();
		$this->headerData['addCss'] = array(
			'assets/css/style.css',
		);
		$this->footerData['addJS'] = array(
			'assets/js/formhandler.js',
		);
		$list_id = segment(3);
		$post = $_POST;
		if (!empty($post)) {
			$this->form_validation->set_rules($this->todo->rules());
			if ($this->form_validation->run() == true) {
				//Setting up completed field as boolean required by database.
				if (isset($post['completed']) && $post['completed'] == "on") {
					$post['completed'] = true;
				} else {
					$post['completed'] = false;
				}
				//Calling API according to the paramater available
				$curl = curl_init();
				if ($list_id == '') {
					$url = 'http://' . $_SERVER['HTTP_HOST'] . '/practicaltaskci/rest-api/todoitem';
					$requestType = "POST";
				} else {
					$url = 'http://' . $_SERVER['HTTP_HOST'] . '/practicaltaskci/rest-api/todoitem/' . $list_id;
					$requestType = "PUT";
				}
				// Seting curl options
				curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $url,
					CURLOPT_HTTPHEADER => array("Accept: application/json"),
					// CURLOPT_POST => 1,
					CURLOPT_CUSTOMREQUEST => $requestType,
					CURLOPT_POSTFIELDS => json_encode($post)
				));
				// Send the request & save response to $resp
				$resp = curl_exec($curl);
				if (curl_errno($curl)) {
					$this->data['error'] = 'Could not complete your request at this time. Please try again later or if the problem persists, please contact for any assistance.';
				} else {
					$returndata = json_decode($resp);
					if ($returndata->status == true) {
						$this->data['success'] = $returndata->message;
					} else {
						$this->data['error'] = $returndata->message;
					}
				}
				curl_close($curl);
			}
		}
		//Edit Mode to get data Starts
		if ($list_id != '') {
			$this->data['header_title'] = "Update Current Item";
			//Calling API fo fetch data according to id
			$curl = curl_init();
			$url = 'http://' . $_SERVER['HTTP_HOST'] . '/practicaltaskci/rest-api/todoitem/' . $list_id;
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url,
			));
			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			if (curl_error($curl)) {
				$returndata = curl_error($curl);
				$this->data['error'] = 'Unable to process your request at this time. Please try again later or contact system administrator for further assistance.';
			} else {
				$returndata = json_decode($resp);
			}
			$this->data['item_data'] = $returndata;
			curl_close($curl);
		} else {
			$this->data['header_title'] = "Add New Item";
		}
		//Edit Mode Ends
		$this->load->view('template/header', $this->headerData);
		$this->load->view('todoform', $this->data);
		$this->load->view('template/footer', $this->footerData);
	}
}
