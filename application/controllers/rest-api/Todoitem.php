<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Todoitem extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('api/todoitem_model', 'todo');
	}

	/**
	 * Get All Data from this method.
	 * Accepts an optional paramenter of id.
	 * If id is set, then response will be data of that particular id only.
	 * If id is not set, then response will be all data.
	 * 
	 * @return Response
	 */
	public function index_get($id = 0)
	{
		$data = $this->todo->getTodoList($id);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	/**
	 * Post data as application/json from this method.
	 * Checks for api validation errors and posts to database
	 *
	 * @return Response
	 */
	public function index_post()
	{
		$input = json_decode($this->security->xss_clean($this->input->raw_input_stream));
		$api_validation_errors = array();
		if ($input->title == "") {
			array_push($api_validation_errors, "title is required");
		}
		if ($input->description == "") {
			array_push($api_validation_errors, "description is required");
		}
		if ($input->date == "") {
			array_push($api_validation_errors, "date is required");
		}
		if (empty($api_validation_errors)) {
			$postedId = $this->todo->postTodoList($input);
			if (is_int($postedId)) {
				$this->response(array(
					'status' => true,
					'message' => 'Item created successfully.',
				), REST_Controller::HTTP_OK);
			} else {
				$this->response(array(
					'status' => false,
					'message' => 'Internal error',
				), REST_Controller::HTTP_OK);
			}
		} else {
			$this->response(array(
				'status' => false,
				'message' => 'API Validation error',
				'validation_errors' => $api_validation_errors
			), REST_Controller::HTTP_OK);
		}
	}

	/**
	 * PUT / Update particular data by id from this method.
	 *
	 * @return Response
	 */
	public function index_put($id)
	{
		$input = json_decode($this->security->xss_clean($this->input->raw_input_stream));
		$api_validation_errors = array();
		if (empty($api_validation_errors)) {
			$numOfRowsUpdated = $this->todo->updateTodoList($input, $id);
			if ($numOfRowsUpdated >= 0) {
				$this->response(array(
					'status' => true,
					'message' => 'Item updated successfully.',
				), REST_Controller::HTTP_OK);
			} else {
				$this->response(array(
					'status' => false,
					'message' => 'Internal Error.',
				), REST_Controller::HTTP_OK);
			}
		}
		else{
			$this->response(array(
				'status' => false,
				'message' => 'API Validation error',
				'validation_errors' => $api_validation_errors
			), REST_Controller::HTTP_OK);
		}
	}

	/**
	 * Delete particular data by id from this method.
	 *
	 * @return Response
	 */
	public function index_delete($id)
	{
		$numOfRowsUpdated = $this->todo->deleteTodoList($id);
		if ($numOfRowsUpdated == 1) {
			$this->response(['Item deleted successfully.'], REST_Controller::HTTP_OK);
		} else {
			$this->response(['Nothing to delete or Internal error.'], REST_Controller::HTTP_OK);
		}
	}
}
