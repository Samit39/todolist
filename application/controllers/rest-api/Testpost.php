<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/MyApiControllerTokenized.php';

class Testpost extends MyApiControllerTokenized
{
	public function __construct()
	{
		parent::__construct();
		// $this->load->helper('JWThelper');
	}

	public function newpost_get()
	{
		try {
			$decodedToken = $this->validateToken(); //information about the decoded token is stored here and error is thrown as exception whcih is catched in catch block
			$data = array(
				'status' => 'test',
				'decoded' => $decodedToken,
				'message' => 'newpostInside',
			);
			$this->response($data, REST_Controller::HTTP_OK);
		} catch (Exception $e) {
			$this->response(array(
				'status' => false,
				'exceptionMessage' => $e->getMessage()
			), REST_Controller::HTTP_UNAUTHORIZED);
		}
	}
}
