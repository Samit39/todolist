<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/MyApiControllerTokenized.php';

use Firebase\JWT\JWT;


class Auth extends MyApiControllerTokenized
{
	public function __construct()
	{
		parent::__construct();
	}

	//Funtion to login by providing username and password
	//login function to get new token and refresh token
	public function login_post()
	{
		$input = json_decode($this->security->xss_clean($this->input->raw_input_stream));
		if ($input->username == "user" && $input->password == "pass") {
			//if username and password is correct
			$user = array(
				'username' => $input->username,
				'userRole' => 'admin'
			);
			$token = $this->generateToken($user); //generate token by passing the payload of user
			$refreshToken = $this->generateRefreshToken($user); //generate refreshToken by passing the payload of user
			$this->response(array(
				'accessToken' => $token,
				'refreshToken' => $refreshToken
			), REST_Controller::HTTP_OK);
		}
	}

	//Funtion to get new access token from current refresh token
	public function token_post()
	{
		$input = json_decode($this->security->xss_clean($this->input->raw_input_stream));
		$refreshToken = $input->refreshToken;
		if ($refreshToken == "") {
			$this->response(array(
				'message' => "refresh token is empty",
			), REST_Controller::HTTP_UNAUTHORIZED);
		} elseif ($this->auth->checkIfRefreshTokenExists($refreshToken)) {
			//Refresh token exists in the database
			//decode refresh token
			try {
				$decodedPayLoadObject = JWT::decode($refreshToken, REFRESH_TOKEN_SECRET, array('HS256')); //decoding payload using refresh token secret
				$newAccessToken = $this->generateToken($decodedPayLoadObject); //generate newAccessToken by passing the payload of user
				$this->response(array(
					'accessToken' => $newAccessToken,
					'message' => 'New Access Token Issued Successfully'
				), REST_Controller::HTTP_OK);
			} catch (Exception $e) {
				$this->response(array(
					'status' => false,
					'exceptionMessage' => $e->getMessage()
				), REST_Controller::HTTP_UNAUTHORIZED);
			}
		} else {
			$this->response(array(
				'message' => 'refresh token invalid / not found'
			), REST_Controller::HTTP_FORBIDDEN);
		}
	}

	public function logout_delete()
	{
		$input = json_decode($this->security->xss_clean($this->input->raw_input_stream));
		$refreshToken = $input->refreshToken;
		if ($refreshToken == "") {
			$this->response(array(
				'message' => "refresh token is empty to logout",
			), REST_Controller::HTTP_UNAUTHORIZED);
		} elseif ($this->auth->checkIfRefreshTokenExists($refreshToken)) {
			//if refresh tokenexists in database
			// delete that token from the database
			$this->auth->removeRefreshToken($refreshToken);
			$this->response(array(
				'message' => 'Logged out Successfully'
			), REST_Controller::HTTP_OK);
		} else {
			$this->response(array(
				'message' => 'refresh token invalid / not found'
			), REST_Controller::HTTP_FORBIDDEN);
		}
	}
}
