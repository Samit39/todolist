<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

use Firebase\JWT\JWT;

class MyApiControllerTokenized extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('api/auth_model', 'auth');
	}

	public function validateToken()
	{
		try {
			$header = $this->input->request_headers(); //Get all headers of the request
			$authorizationHeader = $header['Authorization'];
			$authorizationHeaderArray = explode(" ", $authorizationHeader);
			$extractedToken = (!empty($authorizationHeaderArray) && count($authorizationHeaderArray) >= 2) ? $authorizationHeaderArray[1] : '';
			if ($extractedToken == '') {
				throw new Exception("Token not found");
			} else {
				try {
					$decodedPayLoadObject = JWT::decode($extractedToken, ACCESS_TOKEN_SECRET, array('HS256'));
					return $decodedPayLoadObject;
				} catch (\Exception $e) { // JWT Exception error = mainly token invalid error
					throw new Exception(
						"Invalid or Expired JWT token. Exception Message: " . $e->getMessage() .
							" Line number: " . $e->getLine() .
							" File: " . $e->getFile()
					);
				}
			}
		} catch (Exception $ex) {
			throw new Exception(
				"Exception occured while validating token. Exception Message: " . $ex->getMessage() .
					" Line number: " . $ex->getLine() .
					" File: " . $ex->getFile()
			);
		}
	} //validateToken function ends here

	public function generateToken($userInfo)
	{
		$issuedAt = time();
		// jwt valid for 30 seconds // for example for 60 days = (60 seconds * 60 minutes * 24 hours * 60 days)
		$expirationTime = $issuedAt + 30;
		$payload = array(
			'iat' => $issuedAt,
			'exp' => $expirationTime,
			'userInfo' => $userInfo
		);
		$generatedToken = JWT::encode($payload, ACCESS_TOKEN_SECRET);
		return $generatedToken;
	}

	public function generateRefreshToken($userInfo){
		$generatedRefreshToken = JWT::encode($userInfo, REFRESH_TOKEN_SECRET);
		if($this->auth->checkIfRefreshTokenExists($generatedRefreshToken) == false){
			$this->auth->postTokens(['refresh_tokens' => $generatedRefreshToken]);
		}		
		return $generatedRefreshToken;
	}
}
