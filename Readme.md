# ToDo List - Codeigniter Demonstration
## _The demonstration of TODO application using RESTAPI, AJAX and MYSQL_
## Features

- Simple displaying of the todo list items in default landing page
- Ability to add new items
- Ability to perform AJAX call to API to edit and delete items
- Ability to edit items
- Multiple validations like jQuery Validation (Frontend), codeigniter formvalidation and API data validation

## Tech

This demonstrative codeigniter application uses a number of open source projects to work properly:

- Codeigniter PHP framework
- Bootstrap 3 - For all the front ends
- jQuery
- MySQL
- Bootstrap date picker
- Sweetalert javascript library

## Installation

This application requires PHP V5.6 and greater, MYSQL to run.

Installation process is

```sh
Download the git repository in your local computer.
Navigate to root folder dummy_database and import the sql database to your local mysql.
Navigate to application/config/database.php file and change credentials as per your local.
You may change the project root folder name to practicaltaskci or change the RewriteBase rule on .htaccess file on root level.
Head back to browser and search for http://localhost/practicaltaskci (Dont forget to change your hostname and port.)
```

## Development environment
This application was developed in following environment
- Windows 10
- XAMPP v3.2.4
- PHP v7.4.3
- POSTMAN to test API
- Codeigniter v3.1.11
- Bootstrap v3.3.7

## API Documentation

[API Documentation] - Click to view POSTMAN API documentation or https://documenter.getpostman.com/view/4639355/TzsZrTsM (Copy & Paste this link on browser.)

## License
MIT

**Free Software, Hell Yeah!**

[API Documentation]: <https://documenter.getpostman.com/view/4639355/TzsZrTsM>
