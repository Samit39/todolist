var rootHost = "http://localhost/";
var APIDistributor = "practicaltaskci/rest-api/";
var APIPoint = "todoitem/";
//
var APIEndPoint = rootHost + APIDistributor + APIPoint;
$(document).ready(function () {
	console.log('ready');
	performUpdateCompletedAction(); //Function to change status 
	performDeleteAction(); //Function to delete item

});

//Deletes the item by taking confirmation from user
//Uses AJAX call to API to delete the item
async function performDeleteAction() {
	$('.deleteClass').click(function () {
		swal.fire({
			title: 'Do you want to delete this item?',
			showDenyButton: true,
			showCancelButton: false,
			confirmButtonText: 'Yes, Delete',
			denyButtonText: 'No',
		}).then((result) => {
			if (result.isConfirmed) {
				var id = $(this).attr('id');
				//Perform ajax call on function deleteSingleItem(id)
				if (deleteSingleItem(id)) {
					var elment = $(this).parents();
					elment.eq(1).remove();
					swal.fire('Deleted Successfully!', '', 'success');
				}
				else {
					console.log("Could Not delete");
					swal.fire('Changes are not saved', '', 'info');
				}
			} else if (result.isDenied) {
				swal.fire('Changes are not saved', '', 'info');
			}
		});

	});
}

//Function to delete single item according to id by making an AJAX call to API
async function deleteSingleItem(id) {
	$.ajax({
		type: "DELETE",
		url: APIEndPoint + id,
		dataType: "json",
		contentType: "application/json",
		success: function (returneddata) {
			return true;
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("Oops!", "There was an error processing your request. Please try again.");
			return false;
		}
	});
}

//Function to perform Status Change by calling an API
async function performUpdateCompletedAction() {
	$("input:checkbox").change(function () {
		if ($(this).is(":checked")) {
			newStatus = "Completed";
		}
		else {
			newStatus = "Incomplete";
		}
		//Show dialog box according to the newStatus
		Swal.fire({
			title: 'Do you want to change status to ' + newStatus,
			showDenyButton: true,
			showCancelButton: false,
			confirmButtonText: 'Yes, Change to ' + newStatus,
			denyButtonText: 'No, Dont Change',
		}).then((result) => {
			if (result.isConfirmed) {
				if ($(this).is(":checked")) {
					var data = {
						"completed": true
					}
					//Call function which uses AJAX call to API to change state
					updateItemsState($(this).attr("id"), data);
				} else {
					var data = {
						"completed": false
					}
					//Call function which uses AJAX call to API to change state
					updateItemsState($(this).attr("id"), data);
				}
				Swal.fire('Successfully changed to ' + newStatus, '', 'success')
			} else if (result.isDenied) {
				//Set the status back to previous form in terms of UI view part as clicking will check/uncheck checkbox
				currentStatus = $(this).prop('checked');
				$(this).prop('checked', !currentStatus);
				Swal.fire('Changes are not saved', '', 'info')
			}
		})
	});
}

//Function to change status/update single item according to id by making an AJAX call to API
async function updateItemsState(id, data) {
	$.ajax({
		type: "PUT",
		url: APIEndPoint + id,
		data: JSON.stringify(data),
		dataType: "json",
		contentType: "application/json",

		success: function (returneddata) {
			console.log(returneddata);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("Oops!", "There was an error processing your request. Please try again.");
			console.log(JSON.stringify(jqXHR));
		}
	});
}

//Function to get all items by making an AJAX call to API
function getAllItemsCountFromAPI() {
	$.ajax({
		type: "GET",
		url: APIEndPoint,
		dataType: "json",
		async:true,
		crossDomain: true,
		cache: false,
		processData: false,
		contentType: "application/x-www-form-urlencoded",
		success: function (data) {
			console.log(data.length);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			swal("Oops!", "There was an error processing your request. Please try again.", "error");
		}
	});
	
}
