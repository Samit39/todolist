$(document).ready(function () {
	console.log('ready');
	$(".date").datepicker({
		format: 'yyyy-mm-dd',
	});
	// bindDatePicker();
	// It has the name attribute "registration"
	$("form[name='todo_form']").validate({
		// Specify validation rules
		rules: {
			// The key name on the left side is the name attribute
			// of an input field. Validation rules are defined
			// on the right side
			title: "required",
			description: "required",
			date: "required",
		},
		// Specify validation error messages
		messages: {
			title: "Please enter your title",
			description: "Please enter your description",
			date: "Please enter your date",
		},
		// Make sure the form is submitted to the destination defined
		// in the "action" attribute of the form when valid
		submitHandler: function (form) {
			form.submit();
		}
	});
});
